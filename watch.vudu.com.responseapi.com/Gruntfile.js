'use strict';
module.exports = function(grunt) {

	//Initializing the configuration object
	grunt.initConfig({

		// Task configuration
		modernizr: {

			// [REQUIRED] Path to the build you're using for development.
			"devFile" : "./httpdocs/assets/modernizr/modernizr.js",

			// [REQUIRED] Path to save out the built file.
			"outputFile" : "./httpdocs/js/vendor/modernizr-custom.js",

			// Based on default settings on http://modernizr.com/download/
			"extra" : {
				"shiv" : true,
				"printshiv" : false,
				"load" : true,
				"mq" : false,
				"cssclasses" : true
			},

			// Based on default settings on http://modernizr.com/download/
			"extensibility" : {
				"addtest" : false,
				"prefixed" : false,
				"teststyles" : false,
				"testprops" : false,
				"testallprops" : false,
				"hasevents" : false,
				"prefixes" : false,
				"domprefixes" : false
			},

			// By default, source is uglified before saving
			"uglify" : false,

			// Define any tests you want to implicitly include.
			"tests" : [],

			// By default, this task will crawl your project for references to Modernizr tests.
			// Set to false to disable.
			"parseFiles" : [true],

			// When parseFiles = true, this task will crawl all *.js, *.css, *.scss files, except files that are in node_modules/.
			// You can override this by defining a "files" array below.
			//"files" :  ['./httpdocs/js/*.js'],

			// When parseFiles = true, matchCommunityTests = true will attempt to
			// match user-contributed tests.
			"matchCommunityTests" : false,

			// Have custom Modernizr tests? Add paths to their location here.
			"customTests" : []
		},
		jshint: {
			options: {
				jshintrc: '.jshintrc'
			},
			all: [
				'Gruntfile.js',
				'./httpdocs/js/pages/*.js',
			]
		},
		less: {
			dist: {
				files: {
					'./httpdocs/css/pages/hdx.css': [
						'./httpdocs/less/hdx.less'
					],
					'./httpdocs/css/pages/signup.v3.css': [
						'./httpdocs/less/signupv3.less'
					]
				},
				options: {
					compress: true,
					// LESS source map
					// To enable, set sourceMap to true and update sourceMapRootpath based on your install
					sourceMap: false,
				}
			}
		},
		uglify: {
			dist: {
				files: {
					// MODERNIZR...
					'./httpdocs/js/compiled/modernizr.custom.min.js': [
						"./httpdocs/assets/modernizr/modernizr.js",
						'./httpdocs/js/modernizr-polyfills.js'
					],
					// CORE...
					'./httpdocs/js/compiled/core.min.js': [
						'./httpdocs/assets/jquery-legacy/jquery.js',
						'./httpdocs/assets/momentjs/moment.js',
						'./httpdocs/assets/momentjs/moment-timezone.js'
					],
					// HDX...
					'./httpdocs/js/compiled/hdx.min.js': [
						'./httpdocs/js/pages/hdx.js'
					],

					// SIGNUP...V3
					'./httpdocs/js/compiled/signup.v3.min.js': [
						'./httpdocs/assets/slabText/js/jquery.slabtext.js',
						'./httpdocs/assets/parallax/jquery.imageScroll.js',
						'./httpdocs/js/pages/signupv3.js'
					],
				}
			}
		},
		phpunit: {
			//...
			options: {}
		},
		watch: {
			less: {
				options: {
					livereload: true
				},
				files: [
					'./httpdocs/assets/bootstrap/less/*.less',
					'./httpdocs/less/*.less',
					'./httpdocs/less/**/*.less'

				],
				tasks: ['less'],
			},
			js: {
				options: {
					livereload: true
				},
				files: '<%= jshint.all %>',
				tasks: ['uglify'],
			},
		}
	});

	// Plugin loading
	grunt.loadNpmTasks("grunt-modernizr");
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-less');

	// Register tasks
	grunt.registerTask('default', [
		'less',
		'uglify',
		'concat'
	]);
	grunt.registerTask('dev', [
		'watch'
	]);

};
