<?php

class SignupController extends BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$_data = array(
			'_title' 		=> '5 HDX MOVIES ON US',
			'_description' 	=> 'Pick five 1080p movies from select titles to own when you sign up today',
		);
		Assets::addJs('compiled/modernizr.custom.min.js');
		Assets::addCss('//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css');
		Assets::addCSS('pages/signup.v3.css');
		return View::make('signup.index')->with($_data);
	}
}
