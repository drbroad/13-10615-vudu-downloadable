<?php

class HdxController extends BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$_data = array(
			'_title' 		=> 'VUDU HDX Starter Pack',
			'_description' 	=> 'Get 5 movies in 3 steps',
		);
		Assets::addJs('compiled/modernizr.custom.min.js');
		Assets::addCss('http://fonts.googleapis.com/css?family=Droid+Sans:400,700');
		Assets::addCSS('pages/hdx.css');
		return View::make('hdx.index')->with($_data);
	}

}
