<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| script and css filters
|--------------------------------------------------------------------------
*/
//
//Route::filter('before', function(){
//	// Do stuff before every request to your application...
//	Asset::add('modernizer', 'js/jquery-2.0.0.min.js');
//	Asset::add('style', 'template/style.css');
//	Asset::add('style2', 'css/style.css');});
	global $_config;
	$_config = array(
		'collections' => array(
			'modern' => ['compiled/modernizr.custom.min.js'],
			'normalize' => ['normalize.min.css'],
			'main' => ['main.css'],
		),
	);
/*
|--------------------------------------------------------------------------
| Home page route
|--------------------------------------------------------------------------
*/

Route::get('/', function()
{
	global $_config;
	Assets::add($_config);
	return View::make('pages.home');
});

/*
|--------------------------------------------------------------------------
| Partners page route
|--------------------------------------------------------------------------
*/

Route::get('/partners', function()
{
	global $_config;
	$_data = array(
		'_title' 		=> 'VUDU Partner Promotions',
		'_description' 	=> 'Stay close to your favorite entertainment. Get the full VUDU experience on the following partner applications and players.',
		'_require' 		=>	'partner.js',
		'_scripts' 		=> array(
			'moments' 	=> '/js/vendor/moments/moment.min.js',
			'timezone' 	=> '/js/vendor/moments/moment-timezone.min.js'
		),
		'_css'			=> '/css/pages/partners.css',
	);
	Assets::add($_config);
	Assets::addCSS('pages/partners.css');
	return View::make('pages.partners')->with($_data);
});

Route::get('_stats', function()
{
    return View::make('_stats');
});


/*
|--------------------------------------------------------------------------
| HDX page route
|--------------------------------------------------------------------------
*/
Route::get('/hdx-starter-pack', 'HdxController@index');


/*
|--------------------------------------------------------------------------
| Signup page route
|--------------------------------------------------------------------------
*/
Route::get('/signup', 'SignupController@index');

