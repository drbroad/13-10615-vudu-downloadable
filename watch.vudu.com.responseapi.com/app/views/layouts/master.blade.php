<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>{{ $_title }}</title>
        <meta name="description" content="{{ $_description }}">
        <meta name="viewport" content="width=device-width">

        {{ Assets::css() }}
        {{ Assets::js() }}
    </head>
    <body>
		<script src="/network-scripts/analytics/analytics.js" type="text/javascript"></script>
		<!--[if lt IE 7]>
		    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
		<![endif]-->
		<div id="the_bg"></div>

		@yield('content')

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        		<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

	@if($_scripts)
	@foreach ($_scripts as $script)
		 <script src="{{ $script }}" id="{{ key($_scripts) }}" type="text/javascript"></script>
	@endforeach
	@endif

	<script src="/js/main.js" type="text/javascript"></script>

@if($_require)
	<script src="/js/pages/{{ $_require }}" type="text/javascript"></script>
@endif


    </body>
</html>
