<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>{{ $_title }}</title>
        <meta name="description" content="{{ $_description }}">
        <meta name="viewport" content="user-scalable=no, width=device-width" />


        {{ Assets::css() }}
        {{ Assets::js() }}

	<!--[if IE 8]>
	<script src="/js/vendor/respond.min.js"></script>
	<![endif]-->
	<!--[if IE 6]>
	<script src="/js/DD_belatedPNG.js"></script>
	<script>
	  /* EXAMPLE */
	  DD_belatedPNG.fix('.png_bg');

	  /* string argument can be any CSS selector */
	  /* .png_bg example is unnecessary */
	  /* change it to what suits you! */
	</script>
	<![endif]-->
    </head>
    <body>
		<script src="/network-scripts/analytics/analytics.js" type="text/javascript"></script>
		<!--[if lt IE 7]>
		    <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
		<![endif]-->
		<div id="the_bg"></div>

		@yield('content')

		@yield('footerScripts')

    </body>
</html>
