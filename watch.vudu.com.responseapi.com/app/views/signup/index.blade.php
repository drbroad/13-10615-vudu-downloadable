@extends('layouts.container')

@section('content')

	<div class="nav-wrapper">
		<nav role="main" class="nav-affix">
			<div class="container">
				<p class='pull-right clearifx'>
					<span class="hidden-xs hidden-inline">Sign up today and get 5 movies for free</span>
					<a  href="https://my.vudu.com/AccountSetup.html" target="_blank"class="btn btn-info">Sign Up</a> <span class="pipe"> | </span> <a  href="https://my.vudu.com/MyLogin.html?type=sign_in" target="_blank">Sign In</a>
				</p>
				<a href="https://my.vudu.com/"><img src="/images/signup/logo.png" alt=""></a>
			</div>
		</nav>
	</div>

	<section id="intro" class="bg-cover">
		<div class="container clearfix">
			<div class="row">
				<div class="hidden-xs  col-sm-7 col-sm-offset-0 intro-img"  data-50-top="top:0px;" data--100-top="top:-50px;">
					<img src="/images/signup/intro-hero-transparent.png" class="img-responsive png_bg" alt="">
				</div>
				 <!-- Add the extra clearfix for only the required viewport -->
				<div class="clearfix visible-xs"></div>
				<div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-1 col-lg-4 col-lg-offset-0 intro-copy"   data-50-top="top:0px;" data--100-top="top:-50px;">
					<h1 class="fit-text">
						<span class="slabtext">5 HDX</span>
						<span class="slabtext">Movies on us</span>
					</h1>
					<p class="section-copy">Pick five 1080p movies from select titles
to own when you sign up today.*</p>
					<a  class="btn btn-info btn-lg col-xs-12 col-md-8 col-md-offset-2"  href="https://my.vudu.com/AccountSetup.html" target="_blank">Sign Up</a>

					<div class="col-xs-12 clearfix terms"><p class="small">* <a href="http://www.vudu.com/account_promotion.html" target="_blank">Terms and Conditions</a> apply</p></div>

				</div>
			</div>
		</div>


		<aside class="quicklinks">
			<div class="container">
				<div class="row">
					<h3>Why should you <strong>VUDU</strong>?</h3>
				</div>
			</div>
			<div class="shading">
				<div class="container">
					<div class="row">
						<ul>
							<li class="col-sm-4">
								<a href="#watch-panel" class="icon-watch scroll">Watch Anywhere.<span> Anytime.</span> <i class="fa fa-caret-down"></i></a>
							<li class="col-sm-4">
								<a href="#releases-panel" class="icon-releases scroll">New Releases.<span> Instantly.</span><i class="fa fa-caret-down"></i></a>
							</li>
							<li class="col-sm-4">
								<a href="#experience-panel" class="icon-experience scroll">Ultimate Experience.<span> In 1080p.</span><i class="fa fa-caret-down"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</aside>
	</section>

	<section id="watch" class="img-panel" data-image="/images/signup/panel-watch.jpg" data-image-mobile="/images/signup/panel-watch-mob.jpg"data-width="1600" data-height="800">

	</section>
	<section class="copy-panel copy-watch bg-cover" id="watch-panel">
		<div class="container">
			<div class="box">
				<div class="box-inner">

					<div class="icon-copy icon-section-watch"   >
						<h2>Watch Anywhere. Anytime.</h2>
						<p  class="section-copy">At home or on the go, watch VUDU where you want.</p>
						<p class="watch-copy">VUDU works on most online devices including iPad<sup>&reg;</sup>, Mac and PC, Android<sup>&trade;</sup> devices, Roku<sup>&reg;</sup>, Smart TVs, Playstation<sup>&reg;</sup>, Xbox<sup>&reg;</sup>, Blu-ray players and more..</p>
					</div>
				</div>
			</div>
		</div>
		<a href="#releases-panel" class="next scroll hidden-xs"><img src="/images/signup/next.png" alt=""></a>
	</section>


	<section id="releases" class="img-panel" data-image="/images/signup/panel-releases.jpg" data-image-mobile="/images/signup/panel-releases-mob.jpg" data-width="1600" data-height="800">

	</section>
	<section class="copy-panel copy-releases bg-cover" id="releases-panel">
		<div class="container">
			<div class="box">
				<div class="box-inner">
					<div class="icon-copy icon-section-releases"   >
						<h2>Newest Releases. Instantly.</h2>
						<p  class="section-copy">Watch the latest movies and TV shows, often before
	they release on DVD, Blu-ray and Netflix.</p>
					</div>
				</div>
			</div>
		</div>
		<a href="#experience-panel" class="next scroll hidden-xs"><img src="/images/signup/next.png" alt=""></a>
	</section>

	<section id="experience" class="img-panel" data-image="/images/signup/panel-experience.jpg" data-image-mobile="/images/signup/panel-experience-mob.jpg"  data-width="1600" data-height="1000">

	</section>
	<section class="copy-panel copy-experience bg-cover" id="experience-panel">
		<div class="container">
			<div class="box">
				<div class="box-inner">
					<div class="icon-copy icon-section-experience"   >
						<h2>Ultimate Experience. <span class="clearfix visible-xs visible-sm"></span>In 1080p.</h2>
						<p  class="section-copy">Watch movies in our high-quality HDX format for an unbeatable home theatre experience.</p>
					</div>
				</div>
			</div>
		</div>
		<a href="#signup" class="next scroll hidden-xs"><img src="/images/signup/next.png" alt=""></a>
	</section>

	<section id="signup" class="bg-cover">

	<div class="visible-xs poster-header">

	</div>

		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-xs-12 footer-img hidden-xs">
					<img src="/images/signup/signup-posters.png" alt="" class="img-responsive">
					<!-- img[src="/images/signup/dvd-cover.jpg"].dvd-cover.col-xs-3.col-md-4*12 -->
<!-- 						<img src="/images/signup/dvd-cover.jpg" alt="" class="dvd-cover col-xs-3 col-sm-4">
					<img src="/images/signup/dvd-cover.jpg" alt="" class="dvd-cover col-xs-3 col-sm-4">
					<img src="/images/signup/dvd-cover.jpg" alt="" class="dvd-cover col-xs-3 col-sm-4">
					<img src="/images/signup/dvd-cover.jpg" alt="" class="dvd-cover col-xs-3 col-sm-4">
					<img src="/images/signup/dvd-cover.jpg" alt="" class="dvd-cover col-xs-3 col-sm-4">
					<img src="/images/signup/dvd-cover.jpg" alt="" class="dvd-cover col-xs-3 col-sm-4">
					<img src="/images/signup/dvd-cover.jpg" alt="" class="dvd-cover col-xs-3 col-sm-4">
					<img src="/images/signup/dvd-cover.jpg" alt="" class="dvd-cover col-xs-3 col-sm-4">
					<img src="/images/signup/dvd-cover.jpg" alt="" class="dvd-cover col-xs-3 col-sm-4">
					<img src="/images/signup/dvd-cover.jpg" alt="" class="dvd-cover col-xs-3 col-sm-4">
					<img src="/images/signup/dvd-cover.jpg" alt="" class="dvd-cover col-xs-3 col-sm-4">
					<img src="/images/signup/dvd-cover.jpg" alt="" class="dvd-cover col-xs-3 col-sm-4"> -->
				</div>
				<div class="col-sm-8 footer-copy"  data-bottom-top="margin-top:100px; opacity:0.5;" data-bottom="margin-top:0px; opacity:1;">
					<h2 class="fit-text">
						<span class="font-med">Start Your</span>
						<span class="slabtext">Digital Collection</span>
					</h2>
					<div class="col-xs-8 col-xs-offset-2 col-md-6 col-md-offset-3">
						<h5 class="fit-text font-med">
							<span class="slabtext">Sign up today and get </span>
							<span class="slabtext font-heavy">5 HDX movies on us!*</span>
						</h5>

					</div>

					<!-- Add the extra clearfix for only the required viewport -->
					<div class="clearfix"></div>
					<a  class="btn btn-info btn-lg col-xs-8 col-xs-offset-2 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4"  href="https://my.vudu.com/AccountSetup.html" target="_blank">Sign Up</a>
					<!-- Add the extra clearfix for only the required viewport -->
					<div class="clearfix"></div>
					<div class="col-xs-12 terms"><p class="small">* <a href="http://www.vudu.com/account_promotion.html" target="_blank">Terms and Conditions</a>  apply</p></div>
				</div>

			</div>
		</div>
	</section>
@stop

@section('footerScripts')
	<script src="/js/s_code.js" type="text/javascript"></script>
	<script src="/js/compiled/core.min.js" type="text/javascript"></script>
	<script src="/js/compiled/signup.v3.min.js" type="text/javascript"></script>
@stop
