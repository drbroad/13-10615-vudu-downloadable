@extends('layouts.container')

@section('content')

	<div class="hdx">
		<nav>
			<div class="container">
				<a href="http://www.vudu.com" title="Visit the Vudu homepage"><img src="/images/hdx/logo-header.png" alt="Logo: VUDU"></a>
			</div>
		</nav>

		<div class="container">
			<header>

			</header>

			<aside>
				<h2>Important</h2>
				<p>Keep this page open in order to follow all the steps and redeem your 5 movies for free </p>
			</aside>

			<section class="step step-1">
				<div class="step-header clearfix">
					<h3 class='step-number'>Step <strong>1</strong></h3>
					<p class="blurb">Click the <strong>‘Start Here’</strong> button below to open a new browser window and sign into your VUDU account.  If you’ve forgotten your password, click the green ‘Password’ link.</p>
				</div>

				<a  class="btn btn-info btn-lg" href="https://my.vudu.com/MyLogin.html?type=sign_in&url=https://my.vudu.com/MyAccount.html?page=payment" target="_blank">Start Here</a>
				<p class="arrow">Enter your Email and Password on the next screen and click ‘Sign In’</p>


			</section>

			<section class="step step-2">
				<div class="step-header clearfix">
					<h3 class='step-number'>Step <strong>2</strong></h3>
					<p class="blurb">Once signed in, click on the green “Add a Credit Card” button on the left. Then, fill in both the billing address information and the credit card information on the next page.<br><span class="note">(You will not be charged for these 5 HDX movies)</span></p>
				</div>

				<p class="arrow">Click on <br>“Add Credit Card”</p>

			</section>

			<section class="step step-3">
				<div class="step-header clearfix">
					<h3 class='step-number'>Step <strong>3</strong></h3>
					<p class="blurb">Now, click on the <strong>‘Get Movies’</strong> button below to pick the 5 HDX movies you want to own.  Your credit card will not be charged for these 5 movies. Then, start watching! </p>
				</div>
				<a  class="btn btn-info btn-lg" href="https://my.vudu.com/MyLogin.html?type=sign_in&url=https://my.vudu.com/RedeemSavedPromo.html?promoId=1" target="_blank">Get Movies</a>

				<p class="arrow">Pick your movies <br>and click “Redeem”</p>
			</section>

		</div>
		<footer>
			<div class="container">
				<a href="http://www.vudu.com" title="Visit the Vudu homepage"><img src="/images/hdx/logo-footer.png" alt="Logo: VUDU"></a>
				<p>© Copyright 2014 VUDU, Inc. All Rights Reserved.</p>
			</div>
		</footer>
	</div>


@stop

@section('footerScripts')
	<script src="/js/s_code.js" type="text/javascript"></script>
	<script src="/js/compiled/core.min.js" type="text/javascript"></script>
	<script src="/js/compiled/hdx.min.js" type="text/javascript"></script>
@stop
