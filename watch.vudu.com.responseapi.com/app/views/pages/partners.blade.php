@extends('layouts.master')

@section('content')
		<div id="outer-wrapper">
			<div class="header-container">
				<header class="wrapper clearfix">
					<img src="images/logo.png" width="118" height="66" alt="vudu" />
				</header>
			</div>

			<div class="main-container">
				<div class="main wrapper clearfix">
					<div id="main_text">
						<img src="images/header_text.png" alt="connecting to vudu has never been easier" />
						<p>Stay close to your favorite entertainment. Get the full VUDU experience on the following partner applications and players. </p>
					</div>
					<div id="the_images">
						<div id="top_row">
							<a id="roku" href="http://www.roku.com/?utm_source=vudu&utm_medium=partnerpage&utm_content=specialoffer&utm_campaign=roku2save20" target="_blank"><img id="lil" src="images/partner/Roku-320-180-bf.jpg" alt="roku" /></a>
							<a id="xbox" href="http://www.walmart.com/ip/Xbox-360-w-Xbox-LIVE-12-Month-Gold-Card-Value-Bundle/21144168" target="_blank"><img id="big" src="images/partner/Banner-2-Xbox-One-trial.jpg" /></a>
						</div>
						<div id="btm_row">
							<a id="steals" href="http://store.vizio.com/blackfriday.html" target="_blank"><img class="minis" src="images/partner/Banner_1_200x180_BF_vudu.jpg" alt="visio black friday" /></a>
							<a id="samsung" href="http://www.samsung.com/us/shop/special-offers/" target="_blank"><img class="minis" src="images/partner/samsung200x180.png" alt="samsung special offers" /></a>
							<a href="http://blog.vudu.com/?p=10512" target="_blank"><img class="minis" src="images/partner/eGift-200-185.png" alt="send an egift" /></a>

						</div>
					</div>
				</div> <!-- #main -->

			</div> <!-- #main-container -->

			<div class="footer-container">
				<footer class="wrapper">
					<span>&copy; 2013 VUDU, Inc. All rights reserved. <a href="http://www.vudu.com/termsofservice.html" target="_blank">Terms and Conditions Apply.</a><span>
				</footer>
			</div>
		</div>

@stop
