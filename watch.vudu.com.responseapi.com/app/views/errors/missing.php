
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
		<div id="the_bg"></div>
		<div id="outer-wrapper">
			<div class="header-container">
				<header class="wrapper clearfix">
					<img src="images/logo.png" width="118" height="66" alt="vudu" />
				</header>
			</div>

			<div class="main-container">
				<div class="main wrapper clearfix">
					<div id="main_text">
						404 File not found <?php echo $error; ?>
					</div>
					
				</div> <!-- #main -->
				
			</div> <!-- #main-container -->

			<div class="footer-container">
				<footer class="wrapper">
					<span>&copy; 2013 VUDU, Inc. All rights reserved. <a href="#" >Terms and Conditions Apply.</a><span>
				</footer>
			</div>
		</div>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>

		<script src="js/moment.min.js"></script>
		<script src="js/moment-timezone.min.js"></script>
        <script src="js/main.js"></script>
		
    </body>
</html>
