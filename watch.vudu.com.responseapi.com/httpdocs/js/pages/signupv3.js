var _scrollSpeed = 800;


( function( $ ) {

	$(".fit-text").slabText({precision: 2});
	$navBar = $('nav[role="main"]');
	$navWrapper = $('.nav-wrapper');
	$navWrapper.height(  $navBar.outerHeight(true)  );

	$( window ).resize(function() {
		setPanelHeights()
	});
	setTimeout(setPanelHeights, 100);
	var w = $(window).width() > 600;
	var parallax =  !$("html").hasClass("lt-ie9")  && w;
	var touch = Modernizr.touch;
	$('.img-panel').imageScroll({
		coverRatio: 0.75,
		imageAttribute: (touch === true) ? 'image-mobile' : 'image',
		touch: touch,
		parallax: parallax,
		holderMinHeight: 340
	});

	/**	01. SCROLL TO
	*************************************************** **/
	$.easing.def = "easeOutBack";
	$(".scroll").on("click", function(e) {
		e.preventDefault();

		var href = $(this).attr('href');
		$target = $(href);
		var elem_top = $target.offset()['top'];
		var elem_height = $target.outerHeight(true);
		var viewport_height = $(window).height();
		if(href != '#') {
			// Scroll to the middle of the viewport
			var my_scroll = elem_top - (viewport_height / 2) + (elem_height/2);
			$('html, body').animate( { scrollTop: my_scroll }, _scrollSpeed );
			//scroll(my_scroll)
		}
		return false;

	});

} )( jQuery );


function setPanelHeights(e){
	var viewport = $(window).height();
	//$('.img-panel').height(viewport * 0.75)
	$('.copy-panel').height(viewport * 0.4);
}

var _vuduGetCookie = function(key)	{
                if (document.cookie && document.cookie.length > 0 ) {
                    var start = document.cookie.indexOf(key + '=');
                    if (start == -1) { return null;  }
                    start += key.length + 1;
                    var end = document.cookie.indexOf(";", start);
                    if (end == -1) { end = document.cookie.length; }
                    var value = decodeURIComponent(escape(document.cookie.substr(start, end - start)));
                    return decodeURIComponent(value); // since GWT does a encodeURIComponent by default when storing cookies
                }
                return null;
 };

            s.pageName="vudu:landingpage:sem-2014";

            s.prop1="";

            s.campaign="";

            s.events="";

            s.products="";

            s.eVar1="";

            s.eVar14=_vuduGetCookie("myvudu.userId");

            console.log(_vuduGetCookie())


            /*************   DO NOT CHANGE !  **************/

            var s_code=s.t();if(s_code)document.write(s_code);

        var _getUrlParam = function(name) {
            var params = location.search.substr(location.search.indexOf("?")+1);
            var sval = "";
            params = params.split("&");
            for (var i=0; i<params.length; i++) {
                temp = params[i].split("=");
                if (temp[0].toLowerCase() == name.toLowerCase()) {
                    sval = temp[1];
                    return unescape(sval);
                }
            }
            return null;
        };
        var _vuduStoreCookie = function( name, value, days )  {
            var domain = window.ipadCookieDomain? window.ipadCookieDomain : (config? config.cookieDomain : accountConfig.cookieDomain);
            var exdate=new Date();
            exdate.setDate(exdate.getDate() + days);
            var c_value = escape(value) + "; expires=" + exdate.toUTCString() + "; path=/; domain=" + domain;
            document.cookie = name + "=" + c_value;
        };
        var _vuduStoreReferrerCookies = function() {
            if (document.referrer && document.referrer.length > 0) {
                var referrerUrl = document.referrer;
                var domainRegex = new RegExp(":\/\/(.[^/]+)");
                var referrerDomain = referrerUrl.match(domainRegex)[1];
                var cookieDomain = window.ipadCookieDomain? window.ipadCookieDomain : (config? config.cookieDomain : accountConfig.cookieDomain);
                if (referrerDomain.indexOf(cookieDomain) < 0) {
                    _vuduStoreCookie( "myvudu.referringDomain", referrerDomain, 90);
                }
            }
            var cId = _getUrlParam('cid');
            if (cId && cId.length > 0) {
                var scId = _getUrlParam('scid');
                var kwId = _getUrlParam('kwid');
                _vuduStoreCookie( "myvudu.campaignId", cId + "-" + scId + "-" + kwId, 90);
            }
        };
        _vuduStoreReferrerCookies();
