/* TYPEKIT */
  (function() {
    var config = {
      kitId: 'aoh6cqi',
      scriptTimeout: 3000
    };
    var h=document.getElementsByTagName("html")[0];h.className+=" wf-loading";var t=setTimeout(function(){h.className=h.className.replace(/(\s|^)wf-loading(\s|$)/g," ");h.className+=" wf-inactive"},config.scriptTimeout);var tk=document.createElement("script"),d=false;tk.src='//use.typekit.net/'+config.kitId+'.js';tk.type="text/javascript";tk.async="true";tk.onload=tk.onreadystatechange=function(){var a=this.readyState;if(d||a&&a!="complete"&&a!="loaded")return;d=true;clearTimeout(t);try{Typekit.load(config)}catch(b){}};var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(tk,s)
  })();

var _vuduGetCookie = function(key)	{
                if (document.cookie && document.cookie.length > 0 ) {
                    var start = document.cookie.indexOf(key + '=');
                    if (start == -1) { return null;  }
                    start += key.length + 1;
                    var end = document.cookie.indexOf(";", start);
                    if (end == -1) { end = document.cookie.length; }
                    var value = decodeURIComponent(escape(document.cookie.substr(start, end - start)));
                    return decodeURIComponent(value); // since GWT does a encodeURIComponent by default when storing cookies
                }
                return null;
 };

            s.pageName="vudu:landingpage:hdx-starter-pack-steps";

            s.prop1="";

            s.campaign="";

            s.events="";

            s.products="";

            s.eVar1="";

            s.eVar14=_vuduGetCookie("myvudu.userId");


            /*************   DO NOT CHANGE !  **************/

            var s_code=s.t();if(s_code)document.write(s_code);
